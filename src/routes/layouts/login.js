import React, { Component, Fragment } from "react";
import IntlMessages from "Util/IntlMessages";
import { Row, Card, CardTitle, Form, Label, Input, Button } from "reactstrap";
import { NavLink } from "react-router-dom";

import { Colxx } from "Components/CustomBootstrap";

import { connect } from "react-redux";
import { loginUser } from "Redux/actions";
import Facebook from 'Components/Facebook';

class LoginLayout extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "demo@gogo.com",
      password: "gogo123"
    };
  }
  onUserLogin() {
    this.props.loginUser(this.state, this.props.history);
/*    if (this.state.email !== "" && this.state.password !== "") {
      this.props.loginUser(this.state, this.props.history);*/
    }
  

  componentDidMount() {
    document.body.classList.add("background");
  }
  componentWillUnmount() {
    document.body.classList.remove("background");
  }
  render() {
    return (
      <Fragment>
        <main style={{backgroundImage:"radial-gradient(circle at 11% 100%, #0e2c67, #345aa3)"}}>
          <div className="container">
            <Row className="h-100">
              <Colxx xxs="7" md="6" className="mx-auto my-auto">
                <Card className="auth-card">
                  <div className="form-side">
                    <NavLink to={`/`} className="white">
                      <span className="logo-single" />
                    </NavLink>
                    <div className="d-flex justify-content-between align-items-center">
                      <Facebook/>
                    </div>
                    <CardTitle>
                      <IntlMessages id="Inicia secion" />
                      <IntlMessages id="Registrate" />
                    </CardTitle>
                    <Form>
                      <Label className="form-group has-float-label mb-4">
                        <Input type="email" defaultValue={this.state.email} />
                        <IntlMessages id="Tu correo electronico" />
                      </Label>
                      <Label className="form-group has-float-label mb-4">
                        <Input type="password" />
                        <IntlMessages
                          id="Tu contraseña"
                          defaultValue={this.state.password}
                        />
                      </Label>
                      <div className="d-flex justify-content-between align-items-center">
                        <NavLink to={`/forgot-password`}>
                          <IntlMessages id="user.forgot-password-question" />
                        </NavLink>
                        <Button
                          color="primary"
                          className="btn-shadow"
                          size="lg"
                          onClick={() => this.onUserLogin()}
                        >
                          <IntlMessages id="user.login-button" />
                        </Button>
                      </div>
                    </Form>
                  </div>
                </Card>
              </Colxx>
            </Row>
          </div>
        </main>
      </Fragment>
    );
  }
}
const mapStateToProps = ({ authUser }) => {
  const { user, loading } = authUser;
  return { user, loading };
};

export default connect(
  mapStateToProps,
  {
    loginUser
  }
)(LoginLayout);
