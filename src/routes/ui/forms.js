import React, { Component, Fragment } from "react";
import { injectIntl} from 'react-intl';
import { Colxx, Separator } from "Components/CustomBootstrap";
import BreadcrumbContainer from "Components/BreadcrumbContainer";
import IntlMessages from "Util/IntlMessages";
import {
  Row,
  Card,
  CardBody,
  Input,
  CardTitle,
  FormGroup,
  Label,
  CustomInput,
  Button,
  FormText,
  Form,
  CardSubtitle
} from "reactstrap";
import Select from "react-select";
import CustomSelectInput from "Components/CustomSelectInput";
import DatePicker from "react-datepicker";
import moment from "moment";
import TagsInput from "react-tagsinput";

import {
  AvForm,
  AvGroup,
  AvInput,
  AvFeedback
} from "availity-reactstrap-validation";
import "react-tagsinput/react-tagsinput.css";
import "react-datepicker/dist/react-datepicker.css";
import "rc-switch/assets/index.css";
import "rc-slider/assets/index.css";
import "react-rater/lib/react-rater.css";
import "react-fine-uploader/gallery/gallery.css";

const selectData = [
  { label: "Chocolate", value: "chocolate", key: 0 },
  { label: "Vanilla", value: "vanilla", key: 1 },
  { label: "Strawberry", value: "strawberry", key: 2 },
  { label: "Caramel", value: "caramel", key: 3 },
  { label: "Cookies and Cream", value: "cookiescream", key: 4 },
  { label: "Peppermint", value: "peppermint", key: 5 }
];

class FormsUi extends Component {
  constructor(props) {
    super(props);
    this.handleTagChange = this.handleTagChange.bind(this);
    this.handleTagChangeLabelOver = this.handleTagChangeLabelOver.bind(this);
    this.handleChangeDateLabelOver = this.handleChangeDateLabelOver.bind(this);
    this.handleTagChangeLabelTop = this.handleTagChangeLabelTop.bind(this);
    this.handleChangeLabelTop = this.handleChangeLabelTop.bind(this);
    this.handleChangeDateLabelTop = this.handleChangeDateLabelTop.bind(this);

    this.state = {
      selectedOption: "",
      selectedOptionLabelOver: "",
      selectedOptionLabelTop: "",
      startDate: null,
      startDateLabelOver: null,
      startDateLabelTop: null,
      startDateTime: null,
      startDateRange: null,
      endDateRange: null,
      embeddedDate: moment(),
      tags: [],
      tagsLabelOver: [],
      tagsLabelTop: []
    };
  }

  handleTagChange(tags) {
    this.setState({ tags });
  }

  handleTagChangeLabelOver(tagsLabelOver) {
    this.setState({ tagsLabelOver });
  }

  handleTagChangeLabelTop(tagsLabelTop) {
    this.setState({ tagsLabelTop });
  }

  handleChangeLabelOver = selectedOptionLabelOver => {
    this.setState({ selectedOptionLabelOver });
  };

  handleChangeLabelTop = selectedOptionLabelTop => {
    this.setState({ selectedOptionLabelTop });
  };

  handleChangeDateLabelOver(date) {
    this.setState({
      startDateLabelOver: date
    });
  }
  handleChangeDateLabelTop(date) {
    this.setState({
      startDateLabelTop: date
    });
  }

  render() {
    const {messages} = this.props.intl;
    return (
      <Fragment>
        <Row>
          <Colxx xxs="12">
            <BreadcrumbContainer
              heading={<IntlMessages id="menu.forms" />}
              match={this.props.match}
            />
            <Separator className="mb-5" />
          </Colxx>
        </Row>



















        <Row className="mb-4">
          <Colxx xxs="6">
            <Card>
              <CardBody>
              <CardTitle>
                    <IntlMessages id="Edicion de datos personales" />
                    <Button color="primary">
                      <IntlMessages id="Guardar" />
                    </Button>
                </CardTitle>
                <Form>
                  <FormGroup row>
                    <Colxx sm={12}>
                      <FormGroup>
                        <Label for="exampleAddress2Grid">
                          <IntlMessages id="Nombre" />
                        </Label>
                        <Input
                          type="text"
                          name="exampleAddress2Grid"
                          id="exampleAddress2Grid"
                          placeholder="Nombre"
                        />
                      </FormGroup>
                    </Colxx>
                    <Colxx sm={12}>
                      <FormGroup>
                        <Label for="exampleAddress2Grid">
                          <IntlMessages id="Apellido paterno" />
                        </Label>
                        <Input
                          type="text"
                          name="exampleAddress2Grid"
                          id="exampleAddress2Grid"
                          placeholder="Apellido paterno"
                        />
                      </FormGroup>
                    </Colxx>
                    <Colxx sm={12}>
                      <FormGroup>
                        <Label for="exampleAddress2Grid">
                          <IntlMessages id="Apellido materno" />
                        </Label>
                        <Input
                          type="text"
                          name="exampleAddress2Grid"
                          id="exampleAddress2Grid"
                          placeholder="Apellido materno"
                        />
                      </FormGroup>
                    </Colxx>





                    <Colxx sm={4}>
                      <FormGroup>
                        <Label for="exampleEmailGrid">
                          <IntlMessages id="Fecha de nacimiento" />
                        </Label>
                        <Input
                          type="text"
                          name="exampleTextGrid"
                          id="exampleTextGrid"
                        />
                      </FormGroup>
                    </Colxx>
                    <Colxx sm={4}>
                      <FormGroup>
                        <Label for="exampleEmailGrid">
                          <IntlMessages id="Mes" />
                        </Label>
                        <Input
                          type="text"
                          name="exampleTextGrid"
                          id="exampleTextGrid"
                        />
                      </FormGroup>
                    </Colxx>
                    <Colxx sm={3}>
                      <FormGroup>
                        <Label for="exampleZipGrid">
                          <IntlMessages id="año" />
                        </Label>
                        <Input
                          type="text"
                          name="exampleZipGrid"
                          id="exampleZipGrid"
                        />
                      </FormGroup>
                    </Colxx>











                    <Colxx sm={12}>
                      <FormGroup>
                        <Label for="exampleEmailGrid">
                          <IntlMessages id="Correo electronico" />
                        </Label>
                        <Input
                          type="email"
                          name="exampleEmailGrid"
                          id="exampleEmailGrid"
                          placeholder={messages["forms.email"]}
                        />
                      </FormGroup>
                    </Colxx>




                    <Colxx sm={4}>
                      <FormGroup>
                        <Label for="exampleAddressGrid">
                          <IntlMessages id="Lada" />
                        </Label>
                        <Input
                          type="text"
                          name="exampleAddressGrid"
                          id="exampleAddressGrid"
                          placeholder="Lada"
                        />
                      </FormGroup>
                    </Colxx> 
                    <Colxx sm={5}>
                      <FormGroup>
                        <Label for="exampleAddressGrid">
                          <IntlMessages id="Telefono" />
                        </Label>
                        <Input
                          type="text"
                          name="exampleAddressGrid"
                          id="exampleAddressGrid"
                          placeholder="numero"
                        />
                      </FormGroup>
                    </Colxx>





                    <Colxx sm={12}>
                      <FormGroup>
                        <Label for="exampleAddressGrid">
                          <IntlMessages id="Calle" />
                        </Label>
                        <Input
                          type="text"
                          name="exampleAddressGrid"
                          id="exampleAddressGrid"
                          placeholder="Calle"
                        />
                      </FormGroup>
                    </Colxx>

                    

                    <Colxx sm={4}>
                      <FormGroup>
                        <Label for="exampleEmailGrid">
                          <IntlMessages id="numero exterior" />
                        </Label>
                        <Input
                          type="text"
                          name="exampleTextGrid"
                          id="exampleTextGrid"
                        />
                      </FormGroup>
                    </Colxx>
                    <Colxx sm={4}>
                      <FormGroup>
                        <Label for="exampleEmailGrid">
                          <IntlMessages id="numero interior" />
                        </Label>
                        <Input
                          type="text"
                          name="exampleTextGrid"
                          id="exampleTextGrid"
                        />
                      </FormGroup>
                    </Colxx>


                    <Colxx sm={3}>
                      <FormGroup>
                        <Label for="exampleZipGrid">
                          <IntlMessages id="codigo postal" />
                        </Label>
                        <Input
                          type="text"
                          name="exampleZipGrid"
                          id="exampleZipGrid"
                        />
                      </FormGroup>
                    </Colxx>
                  </FormGroup>

                </Form>
              </CardBody>
            </Card>
          </Colxx>
        </Row>

















      </Fragment>
    );
  }
}
export default  injectIntl(FormsUi)