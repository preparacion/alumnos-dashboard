import React, { Component, Fragment } from "react";
import IntlMessages from "Util/IntlMessages";
import {
  Row,
  Card,
  CardBody,
  CardTitle,
  Badge,
  UncontrolledDropdown,
  DropdownItem,
  DropdownToggle,
  DropdownMenu
} from "reactstrap";
import { NavLink } from "react-router-dom";
import { Colxx, Separator } from "Components/CustomBootstrap";
import BreadcrumbContainer from "Components/BreadcrumbContainer";
import ReactSiemaCarousel from "Components/ReactSiema/ReactSiemaCarousel";
import { ThemeColors } from "Util/ThemeColors";
import PerfectScrollbar from "react-perfect-scrollbar";
import BigCalendar from "react-big-calendar";
import moment from "moment";
import ReactTable from "react-table";
import DataTablePagination from "Components/DataTables/pagination";
import {
  lineChartConfig,
  doughnutChartConfig
} from "Constants/chartConfig";

import {
  LineShadow,
  DoughnutShadow,
} from "Components/Charts";
import eventsData from "Data/events.json";
import ticketsData from "Data/tickets.json";
import productsData from "Data/products.json";
import cakeData from "Data/dashboard.cakes.json";


BigCalendar.momentLocalizer(moment);

const recentOrders = productsData.data.slice(0, 6);
const tickets = ticketsData.data;
const events = eventsData.data;
const dataTableData = productsData.data.slice(0, 12);
const cakes = cakeData.data;

export default class ECommerceDashboard extends Component {
  render() {
    return (
      <Fragment>
        <Row>
        <Colxx lg="8" xl="5" className="mb-4">
            <Card>
              <CardBody>
                <h1>Historia Latinoamericana</h1>
                <Separator className="mb-5" />
                <div className="scroll dashboard-list-with-thumbs">
                  <PerfectScrollbar
                    option={{ suppressScrollX: true, wheelPropagation: false }}
                  >
                    {recentOrders.map((order, index) => {
                      return (
                        <div key={index} className="d-flex flex-row mb-3">
                          <NavLink
                            to="/app/layouts/details"
                            className="d-block position-relative"
                          >
                            <img
                              src={order.img}
                              alt={order.name}
                              className="list-thumbnail border-0"
                            />
                            <Badge
                              key={index}
                              className="position-absolute badge-top-right"
                              color={order.statusColor}
                              pill
                            >
                              {order.status}
                            </Badge>
                          </NavLink>

                          <div className="pl-3 pt-2 pr-2 pb-2">
                            <NavLink to="/app/layouts/details">
                              <div className="pr-4">
                                <p className="text-muted mb-1 text-small">{order.name}</p>
                              </div>
                              <div className="text-primary text-small font-weight-medium d-none d-sm-block">
                                {order.createDate}
                              </div>
                            </NavLink>
                          </div>
                        </div>
                      );
                    })}
                  </PerfectScrollbar>
                </div>
              </CardBody>
            </Card>
          </Colxx>
          <Colxx lg="16" xl="7" className="mb-4">
            <Card>
              <CardBody>
                <h1>Panorama politico</h1>
                <Separator className="mb-5" />
                  
              </CardBody>
            </Card>
          </Colxx>
        </Row>
        
      </Fragment>
    )
  }
}
