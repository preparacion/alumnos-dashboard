import appLocaleData from 'react-intl/locale-data/es';
import esMessages from '../locales/es_MX';

const EsLang = {
    messages: {
        ...esMessages
    },
    locale: 'es-MX',
    data: appLocaleData
};
export default EsLang;